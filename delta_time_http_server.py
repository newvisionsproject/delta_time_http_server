#!/usr/bin/env python
### libraries #################################################################
import RPi.GPIO as GPIO
import time

from http.server import BaseHTTPRequestHandler, HTTPServer
import random



### setup #####################################################################

### gpio setup ###

BUTTON_PIN = 6

GPIO.setmode(GPIO.BCM)
GPIO.setup(BUTTON_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

last_time = time.clock()
delta_time = 0




### classes ###################################################################

class http_server_request_handler(BaseHTTPRequestHandler):

	def do_GET(self):
		global delta_time
		self.send_header('Content-Type', 'application/json')
		self.end_headers
		
		message = '{ "action" : "get_upm" , "payload" : "{ "upm" : ' + str(round(delta_time *1000000)) +' } }'
		self.wfile.write(bytes(message, "utf8"))
		return
		




### functions #################################################################

def my_callback(channel):
	### global variables ###
	global last_time
	global delta_time

	### calc delta time ###
	current_time = time.clock()
	delta_time = current_time - last_time
	last_time = current_time
	
	print("delta time : " + str(round(delta_time *1000000)))

def run():
    print("Starting server...")

    # Server settings
    # Choose ort 8080, for port 80, which is normall used for a http server, you need root access
    server_address = ('192.168.2.115', 8082)
    httpd = HTTPServer(server_address, http_server_request_handler)
    print('running server...')
    httpd.serve_forever()

	
### main ######################################################################
	
GPIO.add_event_detect(BUTTON_PIN, GPIO.FALLING, callback=my_callback, bouncetime=25)

#while True:
#	pass		
run()